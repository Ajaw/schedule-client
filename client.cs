﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using AngleSharp.Parser;
using Newtonsoft.Json;
using Newtonsoft.Json.Schema;
using Newtonsoft.Json.Linq;
using StackExchange.Redis;

namespace WebApplication1
{
    public class client
    {

        private headerStructure first_header_1 = new headerStructure();
        private headerStructure first_header_2 = new headerStructure();

        private headerStructure second_header_1 = new headerStructure();
        private headerStructure second_header_2 = new headerStructure();

        private IEnumerable<Cookie> responseCookies;

        private WebProxy proxy;
        
        public client()
        {
            try
            {
                
                //string proxyUri = string.Format("{0}:{1}", proxyServer.Adress, proxyServerSetting.Port);
                string proxyUri="";
                proxy = new WebProxy(proxyUri,false)
                {
                    UseDefaultCredentials = false,
                };

                CookieContainer cookies = new CookieContainer();
                HttpClientHandler handler = new HttpClientHandler()  
                {
                    Proxy =  proxy,
                    PreAuthenticate = true,
                    UseDefaultCredentials = false,
                };
                handler.UseCookies = true;
                handler.CookieContainer = cookies;

                ServicePointManager.ServerCertificateValidationCallback +=
                  (sender, cert, chain, sslPolicyErrors) => true;

                HttpClient client = new HttpClient(handler);

               
                
              
              
                HttpResponseMessage resposne = client.GetAsync("https://portalpasazera.pl/").Result;
                client.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36");

                
                Uri uri = new Uri("https://portalpasazera.pl");

                responseCookies = cookies.GetCookies(uri).Cast<Cookie>();





                for (int i = 0; i < 1; i++)
                {

                    try
                    {


                        
                        HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://portalpasazera.pl");
                        request.CookieContainer = new CookieContainer();
                        foreach (var item in responseCookies)
                        {
                            request.CookieContainer.Add(item);
                        }

                        request.Proxy = proxy;
                        
                        //  request.CookieContainer.Add(new Cookie { Name = "cookie", Value = "true", Domain = "portalpasazera.pl" });
                        request.AllowAutoRedirect = true;
                        request.UserAgent = "Mozilla / 5.0(Windows NT 10.0; WOW64) AppleWebKit / 537.36(KHTML, like Gecko) Chrome / 57.0.2987.133 Safari / 537.36";
                        request.ProtocolVersion = HttpVersion.Version10;
                        request.Headers.Add("Accept-Encoding", "gzip, deflate, sdch, br");
                        request.Headers.Add("Accept-Language", "pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4");
                        request.Headers.Add("X-Kendo-Ui-Version", "r3-2016-sp2");
                        request.Headers.Add("X-Requested-With", "XMLHttpRequest");
                        request.Headers.Add("fd8b6e", "5340e6");
                        request.Headers.Add("b4e56e", "12abef");
                        request.Host = "portalpasazera.pl";
                        request.Referer = "https://portalpasazera.pl/";
                        request.Accept = "*/*";

                        //   request.Headers.Add("Accept", "*/*");


                        string content;
                        using (var response = (HttpWebResponse)request.GetResponse())
                        using (var streamReader = new StreamReader(response.GetResponseStream()))
                        {
                            content = streamReader.ReadToEnd();

                            var parser = new AngleSharp.Parser.Html.HtmlParser();

                            var documnet = parser.Parse(content);
                            var body = documnet.GetElementsByTagName("body").First().GetElementsByTagName("script")
                                .Where(m => m.GetAttribute("type") == "text/javascript").ToList();

                            var first_header = body[0].TextContent;
                            first_header_1 = getHeader(0, first_header);
                            first_header_2 = getHeader(first_header_1.Index, first_header);
                            var second_header = body[1].TextContent;
                            second_header_1 = getHeader(0, second_header);
                            second_header_2 = getHeader(second_header_1.Index, second_header);

                        }
                        Console.WriteLine("success");
                    }
                    catch (Exception ex)
                    {

                        Console.WriteLine(ex.Message);
                    }


                }



                Console.Read();

            }
            catch (Exception ex)
            {
                Console.WriteLine();
            }
        }

        public string makeGetRequest(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.CookieContainer = new CookieContainer();
            foreach (var item in responseCookies)
            {
                request.CookieContainer.Add(item);
            }

            // var postData = "wprowadzonyTekst=kat";
           // var data = Encoding.UTF8.GetBytes(postData);

            //   request.CookieContainer.Add(new Cookie { Name = "cookie", Value = "true", Domain = "portalpasazera.pl" });
            request.Proxy = this.proxy;
            request.AllowAutoRedirect = true;
            request.Method = "GET";
            request.UserAgent = "Mozilla / 5.0(Windows NT 10.0; WOW64) AppleWebKit / 537.36(KHTML, like Gecko) Chrome / 57.0.2987.133 Safari / 537.36";
            request.ProtocolVersion = HttpVersion.Version10;
            request.Headers.Add("Accept-Encoding", "gzip, deflate, sdch, br");
            request.Headers.Add("Accept-Language", "pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4");
            request.Headers.Add("X-Kendo-Ui-Version", "r3-2016-sp2");
            request.Headers.Add("X-Requested-With", "XMLHttpRequest");
            string s1 = first_header_1.content.ToString();
            string s2 = first_header_2.content.ToString();
            request.Headers.Add(s1, s2);
            request.Headers.Add(second_header_1.content.ToString(), second_header_2.content.ToString());
            request.Host = "portalpasazera.pl";
            request.Referer = "https://portalpasazera.pl/";
            request.Accept = "*/*";
            //request.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
            //request.ContentLength = data.Length;
            //   request.Headers.Add("Accept", "*/*");

            //using (var stream = request.GetRequestStream())
            //{
            //    stream.Write(data, 0, data.Length);
            //}

            string content;
            using (var response = (HttpWebResponse)request.GetResponse())
            using (var streamReader = new StreamReader(response.GetResponseStream()))
            {
              
                content = streamReader.ReadToEnd();
                return content;            
            }
        }


        public string makeGetRequestForPoster(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.CookieContainer = new CookieContainer();
            var respDomain = responseCookies.First().Domain;
            request.CookieContainer.Add(new Cookie("Section_Charts", "0") { Domain = respDomain });
            request.CookieContainer.Add(new Cookie("Section_WP_Parameters", "0") { Domain = respDomain });
            request.CookieContainer.Add(new Cookie("WybraneWojewodztwo", "24") { Domain = respDomain }); 
            request.CookieContainer.Add(new Cookie("Section_WP_ThroughStations", "0") { Domain = respDomain });
            foreach (var item in responseCookies)
            {
                request.CookieContainer.Add(item);
            }

            // var postData = "wprowadzonyTekst=kat";
            // var data = Encoding.UTF8.GetBytes(postData);

            //   request.CookieContainer.Add(new Cookie { Name = "cookie", Value = "true", Domain = "portalpasazera.pl" });
            request.Proxy = proxy;
            request.AllowAutoRedirect = true;
            request.Method = "GET";
            request.UserAgent = "Mozilla / 5.0(Windows NT 10.0; WOW64) AppleWebKit / 537.36(KHTML, like Gecko) Chrome / 57.0.2987.133 Safari / 537.36";
            request.ProtocolVersion = HttpVersion.Version10;
            request.Headers.Add("Accept-Encoding", "gzip, deflate, sdch, br");
            request.Headers.Add("Accept-Language", "pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4");
            request.Headers.Add("X-Kendo-Ui-Version", "r3-2016-sp2");
            request.Headers.Add("X-Requested-With", "XMLHttpRequest");
            string s1 = first_header_1.content.ToString();
            string s2 = first_header_2.content.ToString();
            request.Headers.Add(s1, s2);
            request.Headers.Add(second_header_1.content.ToString(), second_header_2.content.ToString());
            request.Host = "portalpasazera.pl";
            request.Referer = "https://portalpasazera.pl/";
            request.Accept = "*/*";
            //request.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
            //request.ContentLength = data.Length;
            //   request.Headers.Add("Accept", "*/*");

            //using (var stream = request.GetRequestStream())
            //{
            //    stream.Write(data, 0, data.Length);
            //}

            string content;
            using (var response = (HttpWebResponse)request.GetResponse())
            using (var streamReader = new StreamReader(response.GetResponseStream()))
            {

                content = streamReader.ReadToEnd();
                return content;
            }
        }

        public string downloadPDFfileFromStream(string url, string fileName)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.CookieContainer = new CookieContainer();
            var respDomain = responseCookies.First().Domain;
            request.CookieContainer.Add(new Cookie("Section_Charts", "0") { Domain = respDomain });
            request.CookieContainer.Add(new Cookie("Section_WP_Parameters", "0") { Domain = respDomain });
            request.CookieContainer.Add(new Cookie("WybraneWojewodztwo", "24") { Domain = respDomain });
            request.CookieContainer.Add(new Cookie("Section_WP_ThroughStations", "0") { Domain = respDomain });
            foreach (var item in responseCookies)
            {
                request.CookieContainer.Add(item);
            }

          

            // var postData = "wprowadzonyTekst=kat";
            // var data = Encoding.UTF8.GetBytes(postData);

            //   request.CookieContainer.Add(new Cookie { Name = "cookie", Value = "true", Domain = "portalpasazera.pl" });
            request.AllowAutoRedirect = true;
            request.Method = "GET";
            request.UserAgent = "Mozilla / 5.0(Windows NT 10.0; WOW64) AppleWebKit / 537.36(KHTML, like Gecko) Chrome / 57.0.2987.133 Safari / 537.36";
            request.ProtocolVersion = HttpVersion.Version10;
            request.Headers.Add("Accept-Encoding", "gzip, deflate, sdch, br");
            request.Headers.Add("Accept-Language", "pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4");
            request.Headers.Add("X-Kendo-Ui-Version", "r3-2016-sp2");
            request.Headers.Add("X-Requested-With", "XMLHttpRequest");
            string s1 = first_header_1.content.ToString();
            string s2 = first_header_2.content.ToString();
            request.Headers.Add(s1, s2);
            request.Headers.Add(second_header_1.content.ToString(), second_header_2.content.ToString());
            request.Host = "portalpasazera.pl";
            request.Referer = "https://portalpasazera.pl/";
            request.Accept = "*/*";
            //request.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
            //request.ContentLength = data.Length;
            //   request.Headers.Add("Accept", "*/*");

            //using (var stream = request.GetRequestStream())
            //{
            //    stream.Write(data, 0, data.Length);
            //}

            string content;
            using (var response = (HttpWebResponse)request.GetResponse())
            using (var streamReader = new StreamReader(response.GetResponseStream()))
            {
                using (var fileStream = File.Create("pdfs/"+fileName + ".pdf"))
                {
                    streamReader.BaseStream.CopyTo(fileStream);
                    return fileName + ".pdf";
                }
                              
            }
         
        }

        public string makePostRequest(string url, string postData)
        {


            // HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://portalpasazera.pl/WyszukiwaniePolaczen/StacjeFiltrRead");
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.CookieContainer = new CookieContainer();
                foreach (var item in responseCookies)
                {
                    request.CookieContainer.Add(item);
                }

               // var postData = "wprowadzonyTekst=kat";
                var data = Encoding.Default.GetBytes(postData);

                //   request.CookieContainer.Add(new Cookie { Name = "cookie", Value = "true", Domain = "portalpasazera.pl" });
                request.Proxy = this.proxy;
                request.AllowAutoRedirect = true;
                request.Method = "POST";
                request.UserAgent = "Mozilla / 5.0(Windows NT 10.0; WOW64) AppleWebKit / 537.36(KHTML, like Gecko) Chrome / 57.0.2987.133 Safari / 537.36";
                request.ProtocolVersion = HttpVersion.Version10;
                request.Headers.Add("Accept-Encoding", "gzip, deflate, sdch, br");
                request.Headers.Add("Accept-Language", "pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4");
                request.Headers.Add("X-Kendo-Ui-Version", "r3-2016-sp2");
                request.Headers.Add("X-Requested-With", "XMLHttpRequest");
                string s1 = first_header_1.content.ToString();
                string s2 = first_header_2.content.ToString();
                request.Headers.Add(s1, s2);
                request.Headers.Add(second_header_1.content.ToString(), second_header_2.content.ToString());
                request.Host = "portalpasazera.pl";
                request.Referer = "https://portalpasazera.pl/";
                request.Accept = "*/*";
                request.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
                request.ContentLength = data.Length;
                //   request.Headers.Add("Accept", "*/*");

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                string content;
                using (var response = (HttpWebResponse)request.GetResponse())
                using (var streamReader = new StreamReader(response.GetResponseStream()))
                {
                    content = streamReader.ReadToEnd();
                    return content;
                JObject json = (JObject)JsonConvert.DeserializeObject(content);

                    var station_object = json[0].Where(x => x["Nazwa"].ToString() == "Katowice");
            

               
                }
              
            


        }
          




        private headerStructure getHeader(int index,string text)
        {
            var first_index = text.IndexOf("'", index);
            var second_index = text.IndexOf("'", first_index+1);
            return new headerStructure { content = text.Substring(first_index+1, (second_index - first_index)-1), Index = second_index + 1 };

        }

         void GetData(IAsyncResult result)
        {
            result.ToString();
            Console.WriteLine(result.ToString());
        }
    }




}