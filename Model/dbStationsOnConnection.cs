﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestClientRozklad.db_model
{
    public class dbStationsOnConnection
    {

        public int Id { get; set; }
        public int connectionID { get; set; }

        [ForeignKey("connectionID")]
        public dbConnection connection { get; set; }


        public int stationID { get; set; }

        [ForeignKey("stationID")]
        public  dbStation station { get; set; }

        public int stationNumberOnRoute { get; set; }

        public double distanceFromLastStation { get; set; }
    }
}
