﻿using RestClientRozklad.db_model.connectionData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestClientRozklad.db_model
{
    public class dbConnectionMapping
    {
        public int id { get; set; }

        public bool monday { get; set; }
        public bool tuesday { get; set; }
        public bool wenesday { get; set; }
        public bool thursday { get; set; }
        public bool friday { get; set; }
        public bool saturday { get; set; }
        public bool sunday { get; set; }
        public string connection_number { get; set; }
        public bool was_processed { get; set; }

        //public int? connectionToHour_id { get; set; }
        public virtual dbConnectionToHour connectionToHour { get; set; }       
        public virtual dbConnection connection { get; set; }
        public virtual List<dbConnectionAvailability> connectionMapping { get; set; }
        public virtual dbScheduleVersion version { get; set; }

    }
}
