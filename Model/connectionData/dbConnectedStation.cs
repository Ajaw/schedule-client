﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestClientRozklad.db_model.connectionData
{
    public class dbConnectedStation
    {
        public int id { get; set; }

        public int motherStationID { get; set; }

        public int connectedStationID { get; set; }

        public int previousStationID { get; set; }

        public int connectionID { get; set; }

        public bool finalStation { get; set; }
    }
}
