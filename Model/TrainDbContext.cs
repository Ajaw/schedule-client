﻿using System;
using AngleSharp;
using Microsoft.EntityFrameworkCore;
using RestClientRozklad.db_model;
using RestClientRozklad.db_model.connectionData;
using Configuration = System.Configuration.Configuration;
using DbContext = Microsoft.EntityFrameworkCore.DbContext;


namespace WebApplication1.Model
{
    public class TrainDbContext : DbContext
    {
        public TrainDbContext(DbContextOptions<TrainDbContext> options) :base(options)
        {

        }

        public DbSet<dbConnection> Connections { get; set; }

        public DbSet<dbStation> Stations { get; set; }

        public DbSet<dbStationsOnConnection> Stops { get; set; }

        public DbSet<ArrivalHours> StopHours { get; set; }        

        public DbSet<dbConnectionToHour> StopsHoursToConnection { get; set; }

        public DbSet<dbConnectionAvailability> ConnectionAvailability { get; set; }

        public DbSet<dbConnectionMapping> ConnectionMapping { get; set; }

        public DbSet<dbScheduleVersion> ScheduleVersion { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<dbConnection>(
                   entity =>
                   {
                       entity.HasKey(e => e.ID);
                       entity.ToTable("dbConnections");
                   });

            modelBuilder.Entity<dbStation>(entity =>
            {
                entity.HasKey(e => e.ID);
                entity.ToTable("dbStations");
            });
            modelBuilder.Entity<ArrivalHours>(entity =>
            {
                entity.HasKey(e => e.ID);
                entity.ToTable("ArrivalHours");
            });
            modelBuilder.Entity<dbStationsOnConnection>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.ToTable("dbStationsOnConnection");
            });





        }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseMySQL(@"server=192.168.50.4;userid=dominik;password=admin1;database=train_db;CharSet=utf8;SslMode=none;");
            }
        }





    }
}