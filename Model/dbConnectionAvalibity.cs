﻿using RestClientRozklad.db_model.connectionData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestClientRozklad.db_model
{
    public class dbConnectionAvailability
    {

        public int id { get; set; }

        public DateTime beggingDate { get; set; }
        public DateTime endDate { get; set; }
        public bool ignoreWeekDays { get; set; }

        public virtual dbConnectionMapping mapping { get; set; }


    }
}
